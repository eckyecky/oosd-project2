import java.awt.Point;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

/**
 * Implements some monster specific features over unit
 */
public class Monster extends Unit {

	/**
	 * Zombie generator
	 * 
	 * @param world
	 *            Reference to world
	 * @param pos
	 *            starting position
	 * @return Monster object
	 * @throws SlickException
	 */
	public static Monster createZombie(World world, Point pos)
			throws SlickException {
		final int damage = 10;
		final int hp = 60;
		final int cooldown = 800;

		return new Monster(world, "Zombie", damage, hp, cooldown, new Double(
				pos.x, pos.y), Unit.TYPE.zombie);
	}

	/**
	 * skeleton generator
	 * 
	 * @param world
	 *            Reference to world
	 * @param pos
	 *            starting position
	 * @return Monster object
	 * @throws SlickException
	 */
	public static Monster createSkeleton(World world, Point pos)
			throws SlickException {
		final int damage = 16;
		final int hp = 100;
		final int cooldown = 500;

		return new Monster(world, "Skeleton", damage, hp, cooldown, new Double(
				pos.x, pos.y), Unit.TYPE.skeleton);
	}

	/**
	 * Bandit generator
	 * 
	 * @param world
	 *            Reference to world
	 * @param pos
	 *            starting position
	 * @return Monster object
	 * @throws SlickException
	 */
	public static Monster createBandit(World world, Point pos)
			throws SlickException {
		final int damage = 8;
		final int hp = 40;
		final int cooldown = 200;

		return new Monster(world, "Bandit", damage, hp, cooldown, new Double(
				pos.x, pos.y), Unit.TYPE.bandit);
	}

	/**
	 * Draelic generator
	 * 
	 * @param world
	 *            Reference to world
	 * @param pos
	 *            starting position
	 * @return Monster object
	 * @throws SlickException
	 */
	public static Monster createDraelic(World world, Point pos)
			throws SlickException {
		final int damage = 30;
		final int hp = 140;
		final int cooldown = 400;

		return new Monster(world, "Draelic", damage, hp, cooldown, new Double(
				pos.x, pos.y), Unit.TYPE.necromancer);
	}

	/**
	 * Constructs new Monster
	 * 
	 * @param world
	 *            Reference to world
	 * @param name
	 *            Unit Name
	 * @param damage
	 *            Unit max damage
	 * @param maxHP
	 *            Unit max hp
	 * @param cooldown
	 *            unit cooldown
	 * @param pos
	 *            start position
	 * @param type
	 *            unit type
	 * @throws SlickException
	 */
	Monster(World world, String name, int damage, int maxHP, int cooldown,
			Double pos, TYPE type) throws SlickException {
		super(world, name, damage, maxHP, cooldown, pos, type);
		setSpeed(0.25);
	}

	/**
	 * Render unit
	 */
	public void render(Graphics g) {
		super.render(g);
		float healthWidth = Math.max(70, g.getFont().getWidth(getName()) + 6);
		float healthHeight = g.getFont().getHeight(getName());
		float healthX = (float) getBounds().getCenterX() - healthWidth / 2
				- world.getCamera().getMinX();
		float stringX = (float) getBounds().getCenterX()
				- g.getFont().getWidth(getName()) / 2
				- world.getCamera().getMinX();
		float healthY = (float) getBounds().getMinY()
				- world.getCamera().getMinY() - 1 - healthHeight;
		g.setColor(Color.black);
		g.fillRect(healthX, healthY, healthWidth, healthHeight);
		g.setColor(Color.red);
		g.fillRect(healthX, healthY,
				healthWidth * getHealth() / getMaxHealth(), healthHeight);
		g.setColor(Color.white);
		g.drawString(getName(), (int) stringX, (int) healthY);

	}

	/**
	 * define movement
	 * 
	 * @param delta
	 *            timestep (ms)
	 * @param away
	 *            away from player
	 */
	public void move(int delta, boolean away) {
		double distX = world.getPlayer().position.x - position.x;
		double distY = world.getPlayer().position.y - position.y;
		double distTotal = Math.sqrt(distX * distX + distY * distY);
		double dx = distX / distTotal * getSpeed() * delta;
		double dy = distY / distTotal * getSpeed() * delta;
		if (away) {
			dx *= -1;
			dy *= -1;
		}
		move(new Point2D.Double(position.x + dx, position.y + dy));

	}

	/**
	 * update monster state
	 */
	public void update(int delta) {
		super.update(delta);
		double distance = getPosition().distance(
				world.getPlayer().getPosition());
		if (distance < 50) {
			attack();
		} else if (distance <= 150) {
			move(delta, false);
		}
	}

}
