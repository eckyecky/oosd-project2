/* SWEN20003 Object Oriented Software Development
 * RPG Game Engine
 * Author: Daniel Edwards edwardsd
 */

import java.awt.Rectangle;
import java.awt.geom.Rectangle2D.Double;

import org.newdawn.slick.SlickException;

/**
 * Represents the camera that controls our viewpoint.
 */
public class Camera {

	/** The unit this camera is following */
	private Player unitFollow;

	/** The width and height of the screen */
	/** Screen width, in pixels. */
	private final int screenwidth;
	/** Screen height, in pixels. */
	private final int screenheight;

	/** The camera's position in the world, in x and y coordinates. */
	private int xPos;
	private int yPos;
	private World world;
	private Rectangle bounds;

	/**
	 * Create a new World object.
	 * 
	 * @param player
	 *            : Character to follow
	 * @param screenwidth
	 *            : camera width
	 * @param screenheight
	 *            : camera height
	 * @param map
	 *            : map to set camera movement limits
	 */
	public Camera(Player player, int screenwidth, int screenheight, World world) {
		unitFollow = player;
		this.screenheight = screenheight;
		this.screenwidth = screenwidth;
		xPos = (int) (player.getX() - screenwidth / 2);
		yPos = (int) (player.getY() - screenheight / 2);
		this.world = world;
		bounds = new Rectangle(xPos, yPos, screenwidth, screenheight);
	}

	/**
	 * Update the game camera to recentre it's viewpoint around the player
	 */
	public void update() throws SlickException {
		TileMap map = world.getMap();
		xPos = Math.max(0, (int) (unitFollow.getX() - screenwidth / 2));
		xPos = Math.min(map.getMapWidth() - screenwidth, xPos);
		yPos = Math.max(0, (int) (unitFollow.getY() - screenheight / 2));
		yPos = Math.min(map.getMapHeight() - screenheight, yPos);
		bounds.x = xPos;
		bounds.y = yPos;
	}

	/**
	 * Tells the camera to follow a given unit.
	 */
	public void followUnit(Unit unit) throws SlickException {
		this.unitFollow = (Player) unit;

	}

	/**
	 * is a rectangle on screen
	 * @param rect
	 * @return
	 */
	public boolean contains(Double rect) {
		return bounds.intersects(rect);
	}

	// Getters and setters
	public int getxPos() {
		return this.xPos;
	}

	public int getyPos() {
		return this.yPos;
	}

	public int getScreenWidth() {
		return screenwidth;
	}

	public int getScreenHeight() {
		return screenheight;
	}

	/**
	 * Returns the minimum x value on screen
	 */
	public int getMinX() {
		return this.xPos;
	}

	/**
	 * Returns the maximum x value on screen
	 */
	public int getMaxX() {
		return this.xPos + screenwidth;

	}

	/**
	 * Returns the minimum y value on screen
	 */
	public int getMinY() {
		return this.yPos;
	}

	/**
	 * Returns the maximum y value on screen
	 */
	public int getMaxY() {
		return this.yPos + screenheight;
	}

}