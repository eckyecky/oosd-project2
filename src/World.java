/* 433-294 Object Oriented Software Development
 * RPG Game Engine
 * Author: Daniel Edwards edwardsd
 */

import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

/**
 * Represents the entire game world. (Designed to be instantiated just once for
 * the whole game).
 */
public class World {
	private TileMap map;
	private Camera cam;
	private ArrayList<Unit> units = new ArrayList<Unit>();
	// Starting unit coords
	private final Point[] bats = { new Point(1431, 864), new Point(1154, 1321),
			new Point(807, 2315), new Point(833, 2657), new Point(1090, 3200),
			new Point(676, 3187), new Point(836, 3966), new Point(653, 4367),
			new Point(1343, 2731), new Point(1835, 3017),
			new Point(1657, 3954), new Point(1054, 5337), new Point(801, 5921),
			new Point(560, 6682), new Point(1275, 5696), new Point(1671, 5991),
			new Point(2248, 6298), new Point(2952, 6373),
			new Point(3864, 6695), new Point(4554, 6443),
			new Point(4683, 6228), new Point(5312, 6606),
			new Point(5484, 5946), new Point(6371, 5634),
			new Point(5473, 3544), new Point(5944, 3339),
			new Point(6301, 3414), new Point(6388, 1994),
			new Point(6410, 1584), new Point(5314, 274) };
	private final Point[] zombies = { new Point(681, 3201),
			new Point(691, 4360), new Point(2166, 2650), new Point(2122, 2725),
			new Point(2284, 2962), new Point(2072, 4515),
			new Point(2006, 4568), new Point(2385, 4629),
			new Point(2446, 4590), new Point(2517, 4532),
			new Point(4157, 6730), new Point(4247, 6620),
			new Point(4137, 6519), new Point(4234, 6449),
			new Point(5879, 4994), new Point(5954, 4928),
			new Point(6016, 4866), new Point(5860, 4277),
			new Point(5772, 4277), new Point(5715, 4277),
			new Point(5653, 4277), new Point(5787, 797), new Point(5668, 720),
			new Point(5813, 454), new Point(5236, 917), new Point(5048, 1062),
			new Point(4845, 996), new Point(4496, 575), new Point(3457, 273),
			new Point(3506, 779), new Point(3624, 1192), new Point(2931, 1396),
			new Point(2715, 1326), new Point(2442, 1374),
			new Point(2579, 1159), new Point(2799, 1269), new Point(2768, 739),
			new Point(2099, 956) };
	private final Point[] bandits = { new Point(1889, 2581),
			new Point(4502, 6283), new Point(5248, 6581),
			new Point(5345, 6678), new Point(5940, 3412),
			new Point(6335, 3459), new Point(6669, 260), new Point(6598, 339),
			new Point(6598, 528), new Point(6435, 528), new Point(6435, 678),
			new Point(5076, 1082), new Point(5191, 1187),
			new Point(4940, 1175), new Point(4760, 1039), new Point(4883, 889),
			new Point(4427, 553), new Point(3559, 162), new Point(3779, 1553),
			new Point(3573, 1553), new Point(3534, 2464),
			new Point(3635, 2464), new Point(3402, 2861),
			new Point(3151, 2857), new Point(3005, 2997),
			new Point(2763, 2263), new Point(2648, 2263),
			new Point(2621, 1337), new Point(2907, 1270), new Point(2331, 598),
			new Point(2987, 394), new Point(1979, 394), new Point(2045, 693),
			new Point(2069, 1028) };
	private final Point[] skeletons = { new Point(1255, 2924),
			new Point(2545, 4708), new Point(4189, 6585), new Point(5720, 622),
			new Point(5649, 767), new Point(5291, 312), new Point(5256, 852),
			new Point(4790, 976), new Point(4648, 401), new Point(3628, 1181),
			new Point(3771, 1181), new Point(3182, 2892),
			new Point(3116, 3033), new Point(2803, 2901),
			new Point(2850, 2426), new Point(2005, 1524),
			new Point(2132, 1427), new Point(2242, 1343), new Point(2428, 771),
			new Point(2427, 907), new Point(2770, 613), new Point(2915, 477),
			new Point(1970, 553), new Point(2143, 1048) };
	private Player pc;
	private ArrayList<Item> items = new ArrayList<>();

	/*
	 * Create a new World object.
	 * 
	 * @throws SlickException
	 */
	public World() throws SlickException {

		this.map = new TileMap("assets/map.tmx", "assets");
		pc = new Player(this, new Point2D.Double(756, 684));
		for (Point bat : bats)
			units.add(GiantBat.createBat(this, bat));
		for (Point zombie : zombies)
			units.add(Monster.createZombie(this, zombie));
		for (Point bandit : bandits)
			units.add(Monster.createBandit(this, bandit));
		for (Point skeleton : skeletons)
			units.add(Monster.createSkeleton(this, skeleton));
		units.add(Monster.createDraelic(this, new Point(2069, 510)));
		units.add(Villager.createVillager(this, Unit.TYPE.peasant));
		units.add(Villager.createVillager(this, Unit.TYPE.prince));
		units.add(Villager.createVillager(this, Unit.TYPE.shaman));

		units.add(pc);
		items = Item.initItems(this);
		this.cam = new Camera(pc, RPG.screenwidth, RPG.screenheight - 70, this);
	}

	/**
	 * Update the game state for a frame.
	 * 
	 * @param dir_x
	 *            The player's movement in the x axis (-1, 0 or 1).
	 * @param dir_y
	 *            The player's movement in the y axis (-1, 0 or 1).
	 * @param delta
	 *            Time passed since last frame (milliseconds).
	 * @param attack
	 */
	public void update(double dir_x, double dir_y, int delta, boolean attack)
			throws SlickException {
		ArrayList<Unit> dead = new ArrayList<>();
		for (Unit unit : units) {
			if (unit.getType() == Unit.TYPE.player) {
				((Player) unit).update(dir_x, dir_y, delta, attack);
			} else if (unit != null) {
				if (unit.getHealth() == 0) {
					dead.add(unit);
				} else {
					unit.update(delta);
				}
			}
		}
		for (Unit unit : dead) {
			units.remove(unit);
		}
		for (Item item : items) {
			item.check();
		}
		for (Item item : pc.getItems()) {
			items.remove(item);
		}
		cam.update();
	}

	/**
	 * Render the entire screen, so it reflects the current game state.
	 * 
	 * @param g
	 *            The Slick graphics object, used for drawing.
	 */
	public void render(Graphics g) throws SlickException {
		int x, y, sx, sy;
		double height;
		double width;

		// Calculate camera draw coordinates
		x = -cam.getxPos() % map.getTileWidth();
		y = -cam.getyPos() % map.getTileHeight();
		width = Math.ceil((double) (cam.getScreenWidth() - x)
				/ map.getTileWidth());
		height = Math.ceil((double) (cam.getScreenHeight() - y)
				/ map.getTileHeight());
		sx = map.xToTile(cam.getxPos());
		sy = map.yToTile(cam.getyPos());

		map.render(x, y, sx, sy, (int) width, (int) height);
		for (Item item : items) {
			if (cam.contains(item.getBounds())) {
				item.render(g);
			}
		}
		for (Unit unit : units) {
			if (cam.contains(unit.getBounds())) {
				unit.render(g);
			}
		}
	}

	// Getters
	public TileMap getMap() {
		return map;
	}

	public Camera getCamera() {
		return cam;
	}

	public Player getPlayer() {
		return pc;
	}

	public ArrayList<Unit> getUnits() {
		return units;
	}

}
