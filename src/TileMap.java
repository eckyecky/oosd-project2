import java.awt.Point;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;

/**
 * TileMap class
 * 
 * @author daniel
 */
public class TileMap extends TiledMap {
	Rectangle2D.Double rect;

	/**
	 * Generate new TileMap
	 * 
	 * @param ref
	 * @param tileSetsLocation
	 * @throws SlickException
	 */
	public TileMap(String ref, String tileSetsLocation) throws SlickException {
		super(ref, tileSetsLocation);
		rect = new Rectangle2D.Double(0, 0, width * tileWidth, height
				* tileHeight);
	}

	/**
	 * checks if tile is blocked
	 * 
	 * @param tile
	 *            tile to check
	 * @return
	 */
	public boolean blocked(Point tile) {
		return super.getTileProperty(super.getTileId(tile.x, tile.y, 0),
				"block", "0").equals("1");
	}

	/**
	 * checks if position is blocked
	 * 
	 * @param pos
	 *            position to check
	 * @return
	 */
	public boolean blocked(Point2D.Double pos) {
		return blocked(pointToTile(pos));
	}

	/**
	 * converts point to tile
	 * 
	 * @param position
	 * @return
	 */
	public Point pointToTile(Point2D.Double position) {
		return new Point(xToTile(position.x), yToTile(position.y));
	}

	/**
	 * Find tile for y coordinate
	 * 
	 * @param y
	 * @return tile number
	 */
	public int yToTile(double y) {
		int tile = (int) Math.floor(y / getTileHeight());
		if (tile < 0)
			return 0;
		if (tile >= getHeight())
			return getHeight() - 1;
		return tile;
	}

	/**
	 * Find tile for x coordinate
	 * 
	 * @param x
	 * @return tile number
	 */
	public int xToTile(double x) {

		int tile = (int) Math.floor(x / getTileWidth());
		if (tile < 0)
			return 0;
		if (tile >= getWidth())
			return getWidth() - 1;
		return tile;
	}

	/**
	 * Gets x position from tile
	 * 
	 * @param x
	 *            tile x
	 * @return position x
	 */
	public int tileToX(int x) {
		return (int) ((x + .5) * getTileWidth());
	}

	/**
	 * Gets y position from tile
	 * 
	 * @param y
	 *            tile y
	 * @return position y
	 */
	public int tileToY(int y) {
		return (int) ((y + .5) * getTileHeight());
	}

	/**
	 * Converts tile coordinate to pixel coordinate
	 * 
	 * @param tile
	 * @return
	 */
	public Point2D.Double tileToPoint(Point tile) {
		return new Point2D.Double(tileToX(tile.x), tileToY(tile.y));
	}

	/**
	 * Check point is in map
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean contains(double x, double y) {

		return rect.contains(x, y);
	}

	/**
	 * check map contains a rectangle
	 * 
	 * @param bounds
	 * @return
	 */
	public boolean contains(java.awt.geom.Rectangle2D.Double bounds) {
		return rect.contains(bounds);
	}

	/**
	 * Check blocking for a rectangle
	 * 
	 * @param bounds
	 * @return
	 */
	public boolean blocked(java.awt.geom.Rectangle2D.Double bounds) {
		boolean ret = false;
		Point2D.Double check = new Point2D.Double(bounds.getCenterX(),
				bounds.getCenterY());
		// check top
		check.y = bounds.getMinY();
		if (blocked(check))
			return true;
		// check bottom
		check.y = bounds.getMaxY();
		if (blocked(check))
			return true;
		// check left
		check.x = bounds.getMinX();
		if (blocked(check))
			return true;
		// check right
		check.x = bounds.getMaxX();
		if (blocked(check))
			return true;

		return ret;
	}

	// getters
	public int getMapWidth() {
		return width * tileWidth;
	}

	public int getMapHeight() {
		return height * tileHeight;
	}
}
