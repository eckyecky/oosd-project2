import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/**
 * Player class
 */
public class Player extends Unit {
	private Point2D.Double spawn = new Point2D.Double(738, 549);
	private Image panel = new Image("assets/panel.png");
	private ArrayList<Item> items = new ArrayList<>();
	private Hashtable<Item.Affect, Integer> effects = new Hashtable<Item.Affect, Integer>();

	/**
	 * Create new player
	 * 
	 * @param world
	 *            Reference to world
	 * @param pos
	 *            Players starting position
	 * @throws SlickException
	 */
	Player(World world, Point2D.Double pos) throws SlickException {
		super(world, "player", 26, 100, 600, pos, Unit.TYPE.player);
		left = false;
		setSpeed(0.25);
		for (Item.Affect type : Item.Affect.values()) {
			effects.put(type, 0);
		}
	}

	/**
	 * Render player
	 */
	public void render(Graphics g) {
		super.render(g);
		renderPanel(g);
	}

	/**
	 * Attack monsters in reach
	 */
	public void attack() {
		if (count < getCooldown()) {
			return;
		}
		count = 0;
		for (Unit unit : world.getUnits()) {
			switch (unit.getType()) {
			case bandit:
			case dreadbat:
			case necromancer:
			case skeleton:
			case zombie:
				if (position.distance(unit.getPosition()) <= 50) {
					Random num = new Random();
					unit.hit(num.nextInt(getDamage() + 1));
				}
				break;
			default:
				break;
			}
		}
	}

	/**
	 * Update player parameters
	 * 
	 * @param dir_x
	 *            Direction to move x
	 * @param dir_y
	 *            Direction to move y
	 * @param delta
	 *            Timestep in ms
	 * @param attack
	 *            If player should attack
	 */
	public void update(double dir_x, double dir_y, int delta, boolean attack) {
		super.update(delta);
		if (getHealth() <= 0) {
			setHealth(getMaxHealth());
			position = (Point2D.Double) spawn.clone();
			return;
		}

		if (attack) {
			attack();
		}

		double xMove;
		double yMove;
		double newX, newY;

		// Movement size
		xMove = dir_x * delta * getSpeed();
		yMove = dir_y * delta * getSpeed();

		// New location for tests
		newX = getX() + xMove;
		newY = getY() + yMove;

		move(new Point2D.Double(newX, newY));

	}

	/**
	 * Renders the player's status panel.
	 * 
	 * @param g
	 *            The current Slick graphics context.
	 */
	public void renderPanel(Graphics g) {
		// Panel colours
		Color LABEL = new Color(0.9f, 0.9f, 0.4f); // Gold
		Color VALUE = new Color(1.0f, 1.0f, 1.0f); // White
		Color BAR_BG = new Color(0.0f, 0.0f, 0.0f, 0.8f); // Black, transp
		Color BAR = new Color(0.8f, 0.0f, 0.0f, 0.8f); // Red, transp

		// Variables for layout
		String text; // Text to display
		int text_x, text_y; // Coordinates to draw text
		int bar_x, bar_y; // Coordinates to draw rectangles
		int bar_width, bar_height; // Size of rectangle to draw
		int hp_bar_width; // Size of red (HP) rectangle
		int inv_x, inv_y; // Coordinates to draw inventory item

		double health_percent; // Player's health, as a percentage

		// Panel background image
		panel.draw(0, RPG.screenheight - RPG.PANELHEIGHT);

		// Display the player's health
		text_x = 15;
		text_y = RPG.screenheight - RPG.PANELHEIGHT + 25;
		g.setColor(LABEL);
		g.drawString("Health:", text_x, text_y);
		text = getHealth() + "/" + getMaxHealth();

		bar_x = 90;
		bar_y = RPG.screenheight - RPG.PANELHEIGHT + 20;
		bar_width = 90;
		bar_height = 30;
		health_percent = (double) getHealth() / getMaxHealth();
		hp_bar_width = (int) (bar_width * health_percent);
		text_x = bar_x + (bar_width - g.getFont().getWidth(text)) / 2;
		g.setColor(BAR_BG);
		g.fillRect(bar_x, bar_y, bar_width, bar_height);
		g.setColor(BAR);
		g.fillRect(bar_x, bar_y, hp_bar_width, bar_height);
		g.setColor(VALUE);
		g.drawString(text, text_x, text_y);

		// Display the player's damage and cooldown
		text_x = 200;
		g.setColor(LABEL);
		g.drawString("Damage:", text_x, text_y);
		text_x += 80;
		text = (new Integer(getDamage())).toString();
		g.setColor(VALUE);
		g.drawString(text, text_x, text_y);
		text_x += 40;
		g.setColor(LABEL);
		g.drawString("Rate:", text_x, text_y);
		text_x += 55;
		text = (new Integer(getCooldown())).toString();
		g.setColor(VALUE);
		g.drawString(text, text_x, text_y);

		// Display the player's inventory
		g.setColor(LABEL);
		g.drawString("Items:", 420, text_y);
		bar_x = 490;
		bar_y = RPG.screenheight - RPG.PANELHEIGHT + 10;
		bar_width = 288;
		bar_height = bar_height + 20;
		g.setColor(BAR_BG);
		g.fillRect(bar_x, bar_y, bar_width, bar_height);

		inv_x = bar_x;
		inv_y = bar_y + bar_height / 2;
		for (Item item : items) {
			inv_x += item.getBounds().width / 2 + 2;
			item.renderInv(g, inv_x, inv_y);
			inv_x += item.getBounds().width / 2;
		}
	}

	/**
	 * add item to player
	 * 
	 * @param item
	 *            the item to add to player
	 */
	public void give(Item item) {
		items.add(item);
		effects.put(item.getAffect(),
				effects.get(item.getAffect()) + item.getAffectValue());
		if (item.getAffect() == Item.Affect.HP) {
			setHealth(getHealth() + item.getAffectValue());
		}
	}

	// Getters and setters
	public ArrayList<Item> getItems() {
		return items;
	}

	public double getX() {
		return position.getX();
	}

	public double getY() {
		return position.getY();
	}

	public int getDamage() {
		return super.getDamage() + effects.get(Item.Affect.Damage);
	}

	public int getCooldown() {
		return super.getCooldown() + effects.get(Item.Affect.Cooldown);
	}

	public int getMaxHealth() {
		return super.getMaxHealth() + effects.get(Item.Affect.HP);
	}

	// Item tests
	public boolean hasTome() {
		return (effects.get(Item.Affect.Cooldown) != 0);
	}

	public boolean hasSword() {
		return (effects.get(Item.Affect.Damage) != 0);
	}

	public boolean hasAmulet() {
		return (effects.get(Item.Affect.HP) != 0);
	}

	public boolean hasElixir() {
		return (effects.get(Item.Affect.None) != 0);
	}
}
