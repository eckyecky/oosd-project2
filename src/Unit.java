import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Rectangle2D.Double;
import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public abstract class Unit {
	// Unit types
	enum TYPE {
		bandit, dreadbat, necromancer, peasant, player, prince, shaman, skeleton, zombie
	}

	protected int count;
	private TYPE type;
	protected Image sprite;
	protected Image flipped;
	private int health, maxHP, cooldown;
	private int damage;
	private double speed;
	private String name;
	protected Point2D.Double position;
	protected World world;
	private Rectangle2D.Double bounds;
	protected boolean left = false;
	protected int underAttack = 8000;
	String dialogue = null;

	/**
	 * Constructs new unit
	 * 
	 * @param world
	 *            Reference to world
	 * @param name
	 *            Unit Name
	 * @param damage
	 *            Unit max damage
	 * @param maxHP
	 *            Unit max hp
	 * @param cooldown
	 *            unit cooldown
	 * @param pos
	 *            start position
	 * @param type
	 *            unit type
	 * @throws SlickException
	 */
	Unit(World world, String name, int damage, int maxHP, int cooldown,
			Point2D.Double pos, TYPE type) throws SlickException {
		this.world = world;
		this.name = name;
		this.damage = damage;
		health = maxHP;
		this.maxHP = maxHP;
		count = cooldown;
		this.cooldown = cooldown;
		position = pos;
		this.type = type;
		sprite = new Image("assets/units/" + type.name() + ".png");
		flipped = sprite.getFlippedCopy(true, false);
		bounds = new Rectangle2D.Double(pos.x - sprite.getWidth() / 2.0, pos.y
				- sprite.getHeight() / 2.0, sprite.getWidth(),
				sprite.getHeight());
	}

	/**
	 * Safely move to new position
	 * 
	 * @param pos
	 *            position to move to
	 */
	public void move(Point2D.Double pos) {
		Point2D.Double newpos = (Point2D.Double) pos.clone();
		TileMap map = world.getMap();
		// check map bounds
		if (!map.contains(testBounds(newpos.x, getY()))) {
			newpos.x = getX();
		}
		if (!map.contains(testBounds(getX(), newpos.y))) {
			newpos.y = getY();
		}

		// check tile block
		if (map.blocked(testBounds(newpos.x, getY()))) {
			newpos.x = getX();
		}
		if (map.blocked(testBounds(getX(), newpos.y))) {
			newpos.y = getY();
		}

		// Move character
		if (newpos.x < getX()) {
			left = true;
		} else if (newpos.x > getX()) {
			left = false;
		}

		position = new Point2D.Double(newpos.x, newpos.y);
		bounds.x = newpos.x - sprite.getWidth() / 2.0;
		bounds.y = newpos.y - sprite.getHeight() / 2.0;

	}

	/**
	 * Talk needs to be overridden
	 * 
	 * @return Dialogue
	 */
	public String talk() {
		return null;
	}

	/**
	 * Render Unit
	 * 
	 * @param g
	 *            Slick graphics object
	 */
	public void render(Graphics g) {
		Image output;
		if (left) {
			output = flipped;
		} else {
			output = sprite;
		}
		output.drawCentered((int) (getX() - world.getCamera().getMinX()),
				(int) (getY() - world.getCamera().getMinY()));
		if (dialogue != null && count < getCooldown()) {
			float healthWidth = Math
					.max(70, g.getFont().getWidth(dialogue) + 6);
			float healthHeight = g.getFont().getHeight(dialogue);
			float healthX = (float) getBounds().getCenterX() - healthWidth / 2
					- world.getCamera().getMinX();
			float stringX = (float) getBounds().getCenterX()
					- g.getFont().getWidth(dialogue) / 2
					- world.getCamera().getMinX();
			float healthY = (float) getBounds().getMinY()
					- world.getCamera().getMinY() - 1 - healthHeight;
			g.setColor(Color.black);
			g.fillRect(healthX, healthY, healthWidth, healthHeight);
			g.setColor(Color.red);
			g.fillRect(healthX, healthY, healthWidth * getHealth()
					/ getMaxHealth(), healthHeight);
			g.setColor(Color.white);
			g.drawString(dialogue, (int) stringX, (int) healthY);
		}
	}

	/**
	 * Generates test bounds for a given position
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public Rectangle2D.Double testBounds(double x, double y) {
		bounds.x = x - sprite.getWidth() / 2.0;
		bounds.y = y - sprite.getHeight() / 2.0;

		return bounds;
	}

	/**
	 * Update unit state
	 * 
	 * @param delta
	 *            frame timestep in ms
	 */
	public void update(int delta) {
		count = Math.min(getCooldown(), count + delta);
	}

	/**
	 * take damage
	 * 
	 * @param damage
	 *            amount of hp to lose
	 */
	public void hit(int damage) {
		health = Math.max(0, health - damage);
		underAttack = 0;
	}

	/**
	 * attack player if in reach
	 */
	public void attack() {
		if (count < getCooldown()) {
			return;
		}
		count = 0;
		Random num = new Random();
		world.getPlayer().hit(num.nextInt(damage + 1));
	}

	// Getters and setters
	public String getName() {
		return name;
	}

	protected void setHealth(int newHealth) {
		health = newHealth;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	public int getDamage() {
		return damage;
	}

	public Double getBounds() {
		return bounds;
	}

	private double getY() {
		return position.y;
	}

	private double getX() {
		return position.x;
	}

	public Point2D.Double getPosition() {
		return position;
	}

	public TYPE getType() {
		return type;
	}

	public int getMaxHealth() {
		return maxHP;
	}

	public int getCooldown() {
		return cooldown;
	}

	public int getHealth() {
		return health;
	}

}