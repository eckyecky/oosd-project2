import java.awt.Point;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;
import java.util.Random;

import org.newdawn.slick.SlickException;

public class GiantBat extends Monster {
	enum Direction {
		N, S, E, W, NW, NE, SE, SW, NONE
	}

	private Direction direction = Direction.NONE;

	/**
	 * bat generator
	 * 
	 * @param world
	 *            Reference to world
	 * @param pos
	 *            starting position
	 * @return bat object
	 * @throws SlickException
	 */
	public static GiantBat createBat(World world, Point pos)
			throws SlickException {
		final int damage = 0;
		final int hp = 40;
		final int cooldown = 0;

		return new GiantBat(world, "Giant Bat", damage, hp, cooldown,
				new Point2D.Double(pos.x, pos.y), Unit.TYPE.dreadbat);
	}

	/**
	 * Constructs new bat
	 * 
	 * @param world
	 *            Reference to world
	 * @param name
	 *            Unit Name
	 * @param damage
	 *            Unit max damage
	 * @param maxHP
	 *            Unit max hp
	 * @param cooldown
	 *            unit cooldown
	 * @param pos
	 *            start position
	 * @param type
	 *            unit type
	 * @throws SlickException
	 */
	GiantBat(World world, String name, int damage, int maxHP, int cooldown,
			Double pos, TYPE type) throws SlickException {
		super(world, name, damage, maxHP, cooldown, pos, type);
		setSpeed(0.2);
	}

	/**
	 * pick direction to randomly move
	 * 
	 * @return random direction
	 */
	private Direction pickDirection() {
		Random num = new Random();
		Direction[] values = Direction.values();
		int pick = num.nextInt(values.length);
		return values[pick];
	}

	/**
	 * update bat state
	 */
	public void update(int delta) {
		underAttack = Math.min(8000, underAttack + delta);
		if (underAttack < 5000) {
			move(delta, true);
			return;
		} else if (underAttack >= 8000) {
			underAttack = 5000;
			direction = pickDirection();
		}
		move(delta);
	}

	/**
	 * move bat
	 * 
	 * @param delta
	 *            timestep
	 */
	private void move(int delta) {
		double newX = getPosition().x, newY = getPosition().y;
		double diagonalDist = getSpeed() * delta / Math.sqrt(2);
		switch (direction) {
		case NW:
			newX -= diagonalDist;
			newY -= diagonalDist;
			break;
		case E:
			newX += getSpeed() * delta;
			break;
		case N:
			newY -= getSpeed() * delta;
			break;
		case NE:
			newX += diagonalDist;
			newY -= diagonalDist;
			break;
		case S:
			newY += getSpeed() * delta;
			break;
		case SE:
			newX += diagonalDist;
			newY += diagonalDist;
			break;
		case SW:
			newX -= diagonalDist;
			newY += diagonalDist;
			break;
		case W:
			newX -= getSpeed() * delta;
			break;
		default:
			break;
		}
		move(new Point2D.Double(newX, newY));
	}

}
