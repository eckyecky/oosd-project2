import java.awt.Point;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Rectangle2D.Double;
import java.util.ArrayList;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class Item {
	private int affectValue;
	private Affect affect;
	private String name;
	private Image sprite;
	private Point position;
	private World world;

	enum Affect {
		HP, Damage, Cooldown, None
	}

	/**
	 * Create all items
	 * 
	 * @param world
	 *            world to place items in
	 * @return
	 * @throws SlickException
	 */
	public static ArrayList<Item> initItems(World world) throws SlickException {
		ArrayList<Item> ret = new ArrayList<>();
		ret.add(createAmulet(world));
		ret.add(createElixir(world));
		ret.add(createSword(world));
		ret.add(createTome(world));
		return ret;
	}

	// Item generators
	public static Item createAmulet(World world) throws SlickException {
		return new Item(world, "Amulet of Vitality", "amulet.png", 80,
				Affect.HP, new Point(965, 3563));
	}

	public static Item createSword(World world) throws SlickException {
		return new Item(world, "Sword of Strength", "sword.png", 30,
				Affect.Damage, new Point(4791, 1253));
	}

	public static Item createTome(World world) throws SlickException {
		return new Item(world, "Tome of Agility", "book.png", -300,
				Affect.Cooldown, new Point(546, 6707));
	}

	public static Item createElixir(World world) throws SlickException {
		return new Item(world, "Elixir of Life", "elixir.png", 1, Affect.None,
				new Point(1976, 402));
	}

	/**
	 * Item constructor
	 * 
	 * @param world
	 *            world reference
	 * @param name
	 *            item name
	 * @param spriteName
	 *            name of sprite
	 * @param affectValue
	 *            how much it affects a stat
	 * @param type
	 *            affect type
	 * @param point
	 *            location in world
	 * @throws SlickException
	 */
	Item(World world, String name, String spriteName, int affectValue,
			Affect type, Point point) throws SlickException {
		this.world = world;
		this.name = name;
		this.affectValue = affectValue;
		affect = type;
		position = point;
		sprite = (new Image("assets/items/" + spriteName));
	}

	/**
	 * if player is close give item
	 */
	public void check() {

		Player pc = world.getPlayer();
		if (pc.position.distance(position) <= 50) {
			pc.give(this);
		}
	}

	/**
	 * render item in world
	 * 
	 * @param g
	 *            graphics context
	 */
	public void render(Graphics g) {
		sprite.drawCentered((int) (position.x - world.getCamera().getMinX()),
				(int) (position.y - world.getCamera().getMinY()));
	}

	/**
	 * render item in inventory
	 * 
	 * @param g
	 *            graphics context
	 * @param inv_x
	 *            coordinate to draw
	 * @param inv_y
	 *            coordinate to draw
	 */
	public void renderInv(Graphics g, int inv_x, int inv_y) {
		sprite.drawCentered(inv_x, inv_y);
	}

	// getters
	public int getAffectValue() {
		return affectValue;
	}

	public Affect getAffect() {
		return affect;
	}

	public String getName() {
		return name;
	}

	public Image getSprite() {
		return sprite;
	}

	public Double getBounds() {
		return new Rectangle2D.Double(position.x - sprite.getWidth() / 2,
				position.y - sprite.getWidth() / 2, sprite.getWidth(),
				sprite.getHeight());
	}

}
