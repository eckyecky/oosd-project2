import java.awt.geom.Point2D;

import org.newdawn.slick.SlickException;

public class Villager extends Unit {
	/**
	 * Villager generator
	 * 
	 * @param world
	 *            Reference to world
	 * @param type
	 *            Specifies which Villager to create
	 * @return
	 * @throws SlickException
	 */
	public static Villager createVillager(World world, TYPE type)
			throws SlickException {
		String name;
		Point2D.Double pos;
		switch (type) {
		case peasant:
			name = "Garth";
			pos = new Point2D.Double(756, 870);
			break;
		case prince:
			name = "Prince Aldric";
			pos = new Point2D.Double(467, 679);
			break;
		case shaman:
			name = "Elvira";
			pos = new Point2D.Double(738, 549);
			break;
		default:
			return null;

		}
		return new Villager(world, name, 0, 1, 4000, pos, type);
	}

	/**
	 * Constructs new villager
	 * 
	 * @param world
	 *            Reference to world
	 * @param name
	 *            Unit Name
	 * @param damage
	 *            Unit max damage
	 * @param maxHP
	 *            Unit max hp
	 * @param cooldown
	 *            unit cooldown
	 * @param pos
	 *            start position
	 * @param type
	 *            unit type
	 * @throws SlickException
	 */
	Villager(World world, String name, int damage, int maxHP, int cooldown,
			Point2D.Double pos, TYPE type) throws SlickException {
		super(world, name, damage, maxHP, cooldown, pos, type);
	}

	/**
	 * Gets dialogue for unit
	 */
	@Override
	public String talk() {
		Player pc = world.getPlayer();
		if (pc.getPosition().distance(getPosition()) > 50) {
			return null;
		}
		switch (getType()) {
		case peasant:
			if (!pc.hasAmulet()) {
				return "Find the Amulet of Vitality, across the river to the west.";
			} else if (!pc.hasSword()) {
				return "Find the Sword of Strength - cross the river and back, on the east side.";
			} else if (!pc.hasTome()) {
				return "Find the Tome of Agility, in the Land of Shadows.";
			} else {
				return "You have found all the treasure I know of.";
			}
		case prince:
			if (pc.hasElixir()) {
				return "The elixir! My father is cured! Thankyou!";
			} else {
				return "Please seek out the Elixir of Life to cure the king.";
			}
		case shaman:
			if (pc.getHealth() != pc.getMaxHealth()) {
				pc.setHealth(pc.getMaxHealth());
				return "You're looking much healthier now.";
			} else {
				return "Return to me if you ever need healing.";
			}
		default:
			return null;

		}
	}

	/**
	 * Update unit state
	 */
	public void update(int delta) {
		super.update(delta);
		if (count >= getCooldown()
				&& world.getPlayer().getPosition().distance(getPosition()) <= 50) {
			count = 0;
			dialogue = talk();
		}
	}
}
